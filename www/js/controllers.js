angular.module('starter.controllers', ['ionic'])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

})


.factory('HeroDataService', function($q, $timeout, $http) {

  var herosData = {};
  $http.get('herodata.json').success(function(data) {
      herosData = data;
      herosData = herosData.sort(function(a, b) {

      var herosDataA = a.name.toLowerCase();
      var herosDataB = b.name.toLowerCase();

      if(herosDataA > herosDataB) return 1;
      if(herosDataA < herosDataB) return -1;
      return 0;
    });
  });

  var searchHeros = function(searchFilter) {
    console.log('Searching heros for ' + searchFilter);
    var deferred = $q.defer();
    var matches = herosData.filter( function(hero) {
      if(hero.name.toLowerCase().indexOf(searchFilter.toLowerCase()) !== -1 ) return true;
    })
    $timeout( function(){
       deferred.resolve( matches );
    }, 100);
    return deferred.promise;
  };
  return {
    searchHeros : searchHeros
  }
})

.controller('heroCombNoteCtrl', function($scope, $ionicModal, $ionicPopup, HeroDataService) {
  $scope.shouldShowDelete = false;
  $scope.shouldShowReorder = false;
  $scope.listCanSwipe = true
  $scope.heroNote = JSON.parse(window.localStorage['heroNote'] || '[]');
  $scope.heroData;
  $scope.autoCompleteData = { heros : [], search : '' };
  
  $scope.search = function(search) {
    $scope.autoCompleteData.search = search;
    HeroDataService.searchHeros(search).then(
      function(matches) {
        $scope.autoCompleteData.heros = matches;
      }
    );
    if($scope.autoCompleteData.heros){
      return $scope.autoCompleteData.heros[0].name
    }
  }

  $scope.completeHeroName = function(hero, select){
    $scope.heroData.heros[$scope.heroData.heros.indexOf(hero)] = angular.copy(select);
  }

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/hero_comb_add.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  $scope.toggleGroup = function(group) {
    if ($scope.isGroupShown(group)) {
      $scope.shownGroup = null;
    } else {
      $scope.shownGroup = group;
    }
  };
  $scope.isGroupShown = function(group) {
    return $scope.shownGroup === group;
  };
  $scope.heroNoteAddOpen = function() {
    $scope.heroData = {title:'', heros:[
        {name:'선택해주세요',level:1},
        {name:'선택해주세요',level:1},
        {name:'선택해주세요',level:1}
    ]};
    $scope.modal.show();
  };
  $scope.heroNoteAddClose = function() {
    $scope.modal.hide();
  };
  $scope.heroNoteAddActive = function(hero) {
    if(hero.name=='선택해주세요'){hero.name = ''}
    for (var i = $scope.heroData.heros.length - 1; i >= 0; i--) {
      $scope.heroData.heros[i].active = false;
    };
    hero.active = true;
  };
  $scope.addComb = function() {
    if($scope.heroData.title != ''){
      $scope.heroNote.push($scope.heroData);
      $scope.modal.hide();
      window.localStorage['heroNote'] = JSON.stringify($scope.heroNote);
    } else {
      $ionicPopup.alert({
        title: '조합 이름 없음'
      });
    }
  }
  
  
});
