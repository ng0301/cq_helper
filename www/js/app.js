// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic','ionic.service.core', 'starter.controllers'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })



  // 고대던전
  .state('app.ancient_dungen', {
    url: '/ancient_dungen',
    views: {
      'menuContent': {
        templateUrl: 'templates/ancient_dungen.html'
      }
    }
  })
  // 빵던시뮬
  .state('app.bread_simulator', {
    url: '/bread_simulator',
    views: {
      'menuContent': {
        templateUrl: 'templates/bread_simulator.html'
      }
    }
  })
  // 용사 조합 노트
  .state('app.hero_comb_note', {
    url: '/hero_comb_note',
    views: {
      'menuContent': {
        templateUrl: 'templates/hero_comb_note.html',
        controller: 'heroCombNoteCtrl'
      }
    }
  })
  // 시나리오 정보
  .state('app.scenario', {
    url: '/scenario',
    views: {
      'menuContent': {
        templateUrl: 'templates/scenario.html'
      }
    }
  })
  // 영혼의 요새
  .state('app.sprit_fortress', {
    url: '/sprit_fortress',
    views: {
      'menuContent': {
        templateUrl: 'templates/sprit_fortress.html'
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/hero_comb_note');
});
